from datetime import timedelta
import airflow
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from datadagconnectiondb import callandinsert

# Args stating ID, owner, start_date and schedule_interval
args = {
    'id': 1,
    'owner': 'airflow',
    'start_date': airflow.utils.dates.days_ago(2),
    'schedule_interval': timedelta(days=1)
}

# DAG object with id, default_args and schedule interval

dag = DAG(
    'recupdata',
    default_args=args,
    description='Gather Apple stock value and store into mongodb',
    schedule_interval=timedelta(days=1),
)

t1 = PythonOperator(
    task_id='get_api_data',
    python_callable=callandinsert,
    dag=dag,
)