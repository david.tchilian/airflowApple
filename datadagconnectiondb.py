import requests, pymongo, datetime

APIKEY = "de93eb397bd1b4b0a6be0de42a6d79e7"

# mongodb connection
def get_connection():
    try:
        client = pymongo.MongoClient("mongodb://localhost:27017/")
        print("Connected successfully!!!")
    except:
        print("Could not connect to MongoDB")
    return client

client = get_connection()
db = client["projet"]
collection = db["data"]

def get_api_data():
    url = "https://financialmodelingprep.com/api/v3/profile/AAPL?apikey=" + APIKEY
    url2= "https://financialmodelingprep.com/api/v3/rating/AAPL?apikey=" + APIKEY
    response = requests.get(url)
    data = response.json()
    return data

def insert_data(data):
    collection.insert_one(data)

def get_data():
    return collection.find()

def delete_data():
    collection.delete_many({})

def callandinsert():
    data = get_api_data()
    insert_data(data)